﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MSJANDROID.Models
{
    class NombreTelPago
    {
        public int idDetallePago { get; set; }
        public string nombre { get; set; }
        public string tel1 { get; set; }
        public string tel2 { get; set; }
        public string fechaPago { get; set; }

    }
}