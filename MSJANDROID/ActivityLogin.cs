﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace MSJANDROID
{
    [Activity(Label = "ActivityLogin")]
    public class ActivityLogin : Activity
    {

       

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layoutLogin);


            List<string> _ListaLogin = new List<string>();


            EditText
                EditTextUser = FindViewById<EditText>(Resource.Id.etUser),
                EditTextPass = FindViewById<EditText>(Resource.Id.etPass);


            Button ButtonLogin = FindViewById<Button>(Resource.Id.ok);
            _ListaLogin = WSSUserLogin<String>(EditTextUser.Text,EditTextPass.Text);

            // Create your application here
        }


        public List<T> WSSUserLogin<T>( string User, string Pass)
        {
            string result = null;
            pvt.PVT wspvt = new pvt.PVT();
            //VALUES  VALORES QUE ENCVIE EL USUARIO INSERT
            wspvt.Timeout = 600000;
            result = wspvt.login(User,Pass);
            return JsonConvert.DeserializeObject<List<T>>(result);
          
        }
    }
}