﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MSJANDROID
{
   public class Comprador
    {
        public string id_comprador { get; set; }
        public string id_comprador_literal { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string colonia { get; set; }
        public string municipio { get; set; }
        public string beneficiario { get; set; }
        public string sexo { get; set; }
        public string originario { get; set; }
        public string residencia { get; set; }
        public string ocupacion { get; set; }
        public string estado_civil { get; set; }
        public string tel1 { get; set; }
        public string tel2 { get; set; }
    }
}