﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSJANDROID;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSJANDROID.Tests
{
    [TestClass()]
    public class ActivityProntoPagoTests
    {
        [TestMethod()]
        public void BuildMsmProntoPagoTest()
        {
            string nombre= "jose";
            string fecha = "10/10/2010";
            string mensaje = "varname gracias por realizar su pago al dia de vardate";

            string msm = mensaje.Replace("varname",nombre).Replace("vardate", fecha) ;

            Assert.AreEqual(msm, "jose gracias por realizar su pago al dia de 10/10/2010");
        }
    }
}