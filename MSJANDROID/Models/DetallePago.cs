﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MSJANDROID
{
public class DetallePago
    {
       
            public string idDetallePago { get; set; }
            public string idVenta { get; set; }
            public string pagoActual { get; set; }
            public string pagoFinal { get; set; }
            public string idLote { get; set; }
            public string mensualidad { get; set; }
            public string deudaTotal { get; set; }
            public string restoAdeudo { get; set; }
            public string comprador { get; set; }
            public string fechaProximoPago { get; set; }
            public string fechaPago { get; set; }
            public string cuantasPagoMora { get; set; }
            public string cuantasAbonoMora { get; set; }
            public string cuantasPagoMes { get; set; }
            public string cuantasAbonoMes { get; set; }
            public string montoPagoMora { get; set; }
            public string montoAbonoMora { get; set; }
            public string montoPagoMes { get; set; }
            public string montoAbonoMes { get; set; }
            public string administracion { get; set; }
            public string monto { get; set; }
            public string tipo_pago { get; set; }
        

    }
}