﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MSJANDROID
{
   public class Predios
    {
        public string id_predio { get; set; }
        public string id_predio_literal{ get; set; }
        public string nombre_predio { get; set; }
        public string colonia { get; set; }
        public string Municipio { get; set; }
        public string administracion { get; set; }
    }
}