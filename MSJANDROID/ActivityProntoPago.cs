﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using Android.Sax;
using Android.Text;
using Java.Security;
using MSJANDROID.Models;

namespace MSJANDROID
{
    [Activity(Label = "ActivityProntoPago")]
    public class ActivityProntoPago : Activity
    {
        public enum listax 
        {
            SmsSharedPreferences
        }


        protected override void OnCreate(Bundle savedInstanceState)
        {



            base.OnCreate(savedInstanceState);


            SetContentView(Resource.Layout.layoutProntoPago);
            // Create your application here
            EditText
                edittextiddetallepago = FindViewById<EditText>(Resource.Id.editTProntoPago),
                edittextTitle = FindViewById<EditText>(Resource.Id.editTtitle);
            Button
                btnSaveSms = FindViewById<Button>(Resource.Id.btnSaveFormatSms),
                btnPayDate = FindViewById<Button>(Resource.Id.btnPayDay),
                btnPeople = FindViewById<Button>(Resource.Id.btnNameP);



            //cambiar class to enum
            // GetPreference(listax.SmsSharedPreferences.ToString());
            edittextiddetallepago.SetText(GetPreference(SharedPrefrences.SmsProntoPagoSP),TextView.BufferType.Editable);
            edittextiddetallepago.AfterTextChanged += delegate
            {
                btnSaveSms.Text = $"Caracteres   {edittextiddetallepago.Text.Length}   Sms  {Math.Ceiling((decimal)edittextiddetallepago.Text.Length / 160)}  Guardar Sms";
            };
           // edittextiddetallepago.AddTextChangedListener();

            btnSaveSms.Click += delegate
            {
                 SetPreference(SharedPrefrences.SmsProntoPagoSP, edittextiddetallepago.Text);
                 Toast.MakeText(this, "Plantilla Guardada", ToastLength.Long).Show();
            };
            btnPeople.Click += delegate
            {
                edittextiddetallepago.SetText(edittextiddetallepago.Text.Insert(edittextiddetallepago.SelectionStart,"*Nombre de Persona*"),TextView.BufferType.Editable);
                edittextiddetallepago.SetSelection(edittextiddetallepago.Text.Length);
            };
            btnPayDate.Click += delegate
            {
                edittextiddetallepago.SetText(edittextiddetallepago.Text.Insert(edittextiddetallepago.SelectionStart,"*Fecha*"), TextView.BufferType.Editable);
              //  edittextiddetallepago.SetText($"{edittextiddetallepago.Text} *FECHA*", TextView.BufferType.Editable);
                edittextiddetallepago.SetSelection(edittextiddetallepago.Text.Length);
            };
            //public string BuildMsmProntoPago()
            //{

            //    return;
            //}
        }
        /// <summary>
        /// Guarda PReferencias de mensaje Pago
        /// </summary>
        /// <param name="SmsProntoPago"></param>
        public void SetPreference(string PreferenceName,string SmsProntoPago)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutString(PreferenceName, SmsProntoPago);
            editor.Apply();
        }

        public string GetPreference(string PreferenceName)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
            //ISharedPreferencesEditor editor = prefs.Edit();
            return prefs.GetString(PreferenceName, "");
        }
    }
    public sealed  class SharedPrefrences
    {
        public static readonly string
            SmsProntoPagoSP = "SmsProntoPagoSP";
    }
}