﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Telephony;
using System.Threading;
using System.Collections;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Preferences;
using MSJANDROID.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Diagnostics;

namespace MSJANDROID
{

    [Activity(Label = "MSJANDROID", MainLauncher = true)]
    public class MainActivity : Activity
    {
        Android.Widget.Switch SwitchMes1, SwitchMes2, SwitchMes3, SwitchMes4;
        Button btnEnviarSms, btnProntoPago;


        public string query_pagos = "SELECT " +
                                         "DP.idDetallePago, " +
                                         "C.nombre, " +
                                         "C.tel1, " +
                                         "C.tel2, " +
                                         "DP.fechaPago " +
                                         "FROM " +
                                         "DetallePago AS DP " +
                                         "INNER JOIN " +
                                         "(" +
                                         "CompraVenta AS CV, Comprador AS C " +
                                         ") " +
                                         "ON DP.idVenta = CV.id_venta " +
                                         "AND CV.pk_comprador = C.id_comprador " +
                                         "ORDER BY DP.idDetallePago DESC  limit 1";
        string _Mesaje = null;
        int _idDetallePagoFromPreference = 0;
        public static int i = 0;
        string numero = null;
        static List<NombreTelPago> _ListatelNombres = new List<NombreTelPago>();
        //int idDetallePagoFromList = _ListatelNombres[0].idDetallePago;
        //string nombreFromList = _ListatelNombres[0].nombre;
        //string tel1FromList = _ListatelNombres[0].tel1;
        //string tel2FromList = _ListatelNombres[0].tel2;
        SmsManager sms = SmsManager.Default;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);


            SetContentView(Resource.Layout.Main);

          //  Intent intent = new Intent(this, typeof(ActivityLogin));

            btnProntoPago = FindViewById<Button>(Resource.Id.btnProntoPago);
            btnEnviarSms = FindViewById<Button>(Resource.Id.btnEnviarSms);
            SwitchMes1 = FindViewById<Android.Widget.Switch>(Resource.Id.swProntoPago);
            //SwitchMes2 = FindViewById<Android.Widget.Switch>(Resource.Id.SWMES2);
            //SwitchMes3 = FindViewById<Android.Widget.Switch>(Resource.Id.SWMES3);
            //SwitchMes4 = FindViewById<Android.Widget.Switch>(Resource.Id.SWProntoPago);


            btnProntoPago.Click += delegate
            {
                // GuardarPreferencias(1234567);
                Intent intent = new Intent(this, typeof(ActivityProntoPago));

                StartActivity(intent);
            };

            btnEnviarSms.Click += delegate
            {
                Stopwatch tiempo = Stopwatch.StartNew();

                // Código del programa...


                if (SwitchMes1.Checked)
                {
                    //si esta el boto 1
                }


                //var  _idDetallePagoFromList = Listatelnombres.Select(L => L.idDetallePago).FirstOrDefault();
                //var _fechaPagoFromList = Listatelnombres.Select(L => L.fechaPago).FirstOrDefault();
                //var nombreFromList = Listatelnombres.Select(L => L.nombre).FirstOrDefault();
                //var tel1FromList = Listatelnombres.Select(L => L.tel1).FirstOrDefault();
                //var tel2FromList = Listatelnombres.Select(L => L.tel2).FirstOrDefault();



              
                tiempo.Stop();
               Console.Write("TIEMPO: " + tiempo.Elapsed.Seconds.ToString());
            };
        }
        /// <summary>
        ///resibe mensaje mas de 170 carcateres y divide el mensaje en mensajes de 160 caracteres
        /// </summary>
        /// <param name="mensaje">string</param>
        public void Dividesms(string mensaje, string numero)
       {
            int length;
            int chartLimit = 160;
            decimal numeroSms = 0;
            List<string> lista = new List<string>();
            length = mensaje.Length;

            if (length <= chartLimit)
            {
                sms.SendTextMessage(numero, null, mensaje, null, null);
                Thread.Sleep(2000);
            }
            else
            {
                numeroSms = Math.Ceiling(Convert.ToDecimal(length) / Convert.ToDecimal(chartLimit));

                for (int i = 0; i < numeroSms; i++)
                {
                    sms.SendTextMessage(numero, null, mensaje.Substring(0, chartLimit), null, null);
                   
                    Thread.Sleep(2000);

                    mensaje = mensaje.Remove(0, chartLimit);
                    length = mensaje.Length;

                    if (length < chartLimit)
                    {
                        chartLimit = length;
                    }
                }
            }
        }

        /// <summary>
        /// Consume metodo de WebService y crea una lista con los resultados de la Query enviada
        /// </summary>
        /// <typeparam name="T">Lista</typeparam>
        /// <returns>lista de Compadores que pagaron</returns>
        public List<T> QueryWsToList<T>()
        {
            string result = null;
            pvt.PVT wspvt = new pvt.PVT();
            //VALUES  VALORES QUE ENCVIE EL USUARIO INSERT
            wspvt.Timeout = 600000;
            result = wspvt.query("Comprador", "", "free", query_pagos);
            return JsonConvert.DeserializeObject<List<T>>(result);
        }
        /// <summary>
        /// retorna el id guardado preferencias de la APP de la ultima Query 
        /// </summary>
        /// <returns>_idDetallePago</returns>
        public int CargarPreferencias()
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
            ISharedPreferencesEditor editor = prefs.Edit();

            int idDetallePago = prefs.GetInt("idDetallePago",0);
            return idDetallePago;
        }
           /// <summary>
           /// Guarda el id de pago de la Query
           /// </summary>
           /// <param name="idDetallePago"></param>
        public void GuardarPreferencias(int idDetallePago)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutInt("idDetallePago", idDetallePago);
            editor.Apply();
        }
        public void ProntoPago()
        {
            //MEtruena al querere crear bariables que no existen
            int idDetallePagoFromList = _ListatelNombres[0].idDetallePago;
            string nombreFromList = _ListatelNombres[0].nombre;
            string tel1FromList = _ListatelNombres[0].tel1;
            string tel2FromList = _ListatelNombres[0].tel2;

            _ListatelNombres = QueryWsToList<NombreTelPago>();
            idDetallePagoFromList = _ListatelNombres[0].idDetallePago;
            nombreFromList = _ListatelNombres[0].nombre;
            tel1FromList = _ListatelNombres[0].tel1;
            tel2FromList = _ListatelNombres[0].tel2;

            _idDetallePagoFromPreference = CargarPreferencias();
            if (idDetallePagoFromList != _idDetallePagoFromPreference)
            {
                //como usar una propierty de sms
                string mensaje;

                mensaje = String.Format("{0}Gracias por realizar su pago", nombreFromList);
                //mensaje.Replace("*Nombre de Persona*",_nombreFromList).Replace("*Fecha*", fecha) ;
                if (tel1FromList == tel2FromList && tel1FromList.Length == 10)
                {
                    Dividesms(mensaje, tel1FromList);
                }
                if (tel1FromList != tel2FromList)
                {
                    if (tel1FromList.Length == 10)
                    {
                        Dividesms(mensaje, tel1FromList);
                    }

                    if (tel2FromList.Length == 10)
                    {
                        Dividesms(mensaje, tel2FromList);
                    }
                }
            }
            GuardarPreferencias(idDetallePagoFromList);
        }
    }


}
    



